#include <uvw.hpp>
#include <memory>

void listen(uvw::Loop &loop) {
    std::shared_ptr<uvw::TCPHandle> tcp = loop.resource<uvw::TCPHandle>();
    tcp->on<uvw::ErrorEvent>([](const uvw::ErrorEvent & er, uvw::TCPHandle &) { exit(er.code()); });
    tcp->on<uvw::ListenEvent>([](const uvw::ListenEvent &, uvw::TCPHandle &srv) {
        printf("Connected to client");
        std::shared_ptr<uvw::TCPHandle> client = srv.loop().resource<uvw::TCPHandle>();
        client->once<uvw::EndEvent>([](const uvw::EndEvent &, uvw::TCPHandle &client) {
            printf("End event\n");
            client.close();
        });
        client->on<uvw::DataEvent>([](const uvw::DataEvent & data, uvw::TCPHandle &) {
            printf("\ndata length %s\n",data.data.get());
            /* data received */ });
        srv.accept(*client);
        client->read();
    });

    tcp->bind("127.0.0.1", 4242);
    tcp->listen();
}

int main() {
    auto loop = uvw::Loop::getDefault();
    listen(*loop);
    loop->run();
}