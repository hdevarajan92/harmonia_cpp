#include <uvw.hpp>
#include <memory>
#include <cstring>

void conn(uvw::Loop &loop) {
    auto tcp = loop.resource<uvw::TCPHandle>();

    tcp->on<uvw::ErrorEvent>([](const uvw::ErrorEvent & er, uvw::TCPHandle &) { exit(er.code()); });

    tcp->once<uvw::ConnectEvent>([](const uvw::ConnectEvent &, uvw::TCPHandle &tcp) {
        char* a=(char*)malloc(10);
        strcpy(a,"hello");
        auto dataWrite = std::unique_ptr<char[]>(strdup(a));
        tcp.write(std::move(dataWrite), strlen(a));
        tcp.close();
        free(a);

    });
    tcp->connect(std::string{"127.0.0.1"}, 4242);
}

int main() {
    auto loop = uvw::Loop::getDefault();
    conn(*loop);
    loop->run();
}